package com.upper.driver.common.swipe_button;

public interface OnActiveListener {
    void onActive();
}
