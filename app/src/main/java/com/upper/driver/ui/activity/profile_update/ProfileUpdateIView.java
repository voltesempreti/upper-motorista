package com.upper.driver.ui.activity.profile_update;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.UserResponse;

public interface ProfileUpdateIView extends MvpView {

    void onSuccess(UserResponse user);

    void onSuccessUpdate(UserResponse object);

    void onError(Throwable e);

    void onSuccessPhoneNumber(Object object);

    void onVerifyPhoneNumberError(Throwable e);

}
