package com.upper.driver.ui.activity.upcoming_detail;


import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.HistoryDetail;

public interface UpcomingTripDetailIView extends MvpView {

    void onSuccess(HistoryDetail historyDetail);
    void onError(Throwable e);
}
