package com.upper.driver.ui.activity.profile;

import com.upper.driver.base.MvpPresenter;

public interface ProfileIPresenter<V extends ProfileIView> extends MvpPresenter<V> {

    void getProfile();

}
