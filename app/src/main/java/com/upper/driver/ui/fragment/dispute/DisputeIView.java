package com.upper.driver.ui.fragment.dispute;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.DisputeResponse;

import java.util.List;

public interface DisputeIView extends MvpView {

    void onSuccessDispute(List<DisputeResponse> responseList);

    void onSuccess(Object object);

    void onError(Throwable e);
}
