package com.upper.driver.ui.fragment.status_flow;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.TimerResponse;

public interface StatusFlowIView extends MvpView {

    void onSuccess(Object object);

    void onWaitingTimeSuccess(TimerResponse object);

    void onError(Throwable e);
}
