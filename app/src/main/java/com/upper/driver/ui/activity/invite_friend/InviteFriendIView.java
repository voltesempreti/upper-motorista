package com.upper.driver.ui.activity.invite_friend;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.UserResponse;

public interface InviteFriendIView extends MvpView {

    void onSuccess(UserResponse response);
    void onError(Throwable e);

}
