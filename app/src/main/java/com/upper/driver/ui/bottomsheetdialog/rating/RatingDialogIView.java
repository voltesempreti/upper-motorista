package com.upper.driver.ui.bottomsheetdialog.rating;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.Rating;

public interface RatingDialogIView extends MvpView {

    void onSuccess(Rating rating);
    void onError(Throwable e);
}
