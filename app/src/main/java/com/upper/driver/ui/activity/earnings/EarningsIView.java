package com.upper.driver.ui.activity.earnings;


import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.EarningsList;

public interface EarningsIView extends MvpView {

    void onSuccess(EarningsList earningsLists);

    void onError(Throwable e);
}
