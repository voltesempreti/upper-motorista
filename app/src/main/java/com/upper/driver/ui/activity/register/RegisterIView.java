package com.upper.driver.ui.activity.register;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.SettingsResponse;
import com.upper.driver.data.network.model.User;
import com.upper.driver.data.network.model.City;
import com.upper.driver.data.network.model.State;
import java.util.List;

public interface RegisterIView extends MvpView {

    void onSuccess(User user);

    void onSuccess(Object verifyEmail);

    void onSuccess(SettingsResponse response);

    void onError(Throwable e);

    void onSuccessPhoneNumber(Object object);

    void onVerifyPhoneNumberError(Throwable e);

    void onVerifyEmailError(Throwable e);

    void onSuccessStates(List<State> states);

    void onSuccessCities(List<City> cities);

}
