package com.upper.driver.ui.fragment.upcoming;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.HistoryList;

import java.util.List;

public interface UpcomingTripIView extends MvpView {

    void onSuccess(List<HistoryList> historyList);
    void onError(Throwable e);
}
