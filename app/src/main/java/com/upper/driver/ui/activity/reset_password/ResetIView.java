package com.upper.driver.ui.activity.reset_password;

import com.upper.driver.base.MvpView;

public interface ResetIView extends MvpView{

    void onSuccess(Object object);
    void onError(Throwable e);
}
