package com.upper.driver.ui.activity.welcome;

import com.upper.driver.base.MvpPresenter;

public interface WelcomeIPresenter<V extends WelcomeIView> extends MvpPresenter<V> {
}
