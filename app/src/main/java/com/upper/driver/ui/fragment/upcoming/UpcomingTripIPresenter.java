package com.upper.driver.ui.fragment.upcoming;


import com.upper.driver.base.MvpPresenter;

public interface UpcomingTripIPresenter<V extends UpcomingTripIView> extends MvpPresenter<V> {

    void getUpcoming();

}
