package com.upper.driver.ui.activity.help;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.Help;

public interface HelpIView extends MvpView {

    void onSuccess(Help object);

    void onError(Throwable e);
}
