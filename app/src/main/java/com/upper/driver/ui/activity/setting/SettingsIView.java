package com.upper.driver.ui.activity.setting;

import com.upper.driver.base.MvpView;

public interface SettingsIView extends MvpView {

    void onSuccess(Object o);

    void onError(Throwable e);

}
