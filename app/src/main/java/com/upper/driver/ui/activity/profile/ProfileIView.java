package com.upper.driver.ui.activity.profile;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.UserResponse;

public interface ProfileIView extends MvpView {

    void onSuccess(UserResponse user);

    void onError(Throwable e);

}
