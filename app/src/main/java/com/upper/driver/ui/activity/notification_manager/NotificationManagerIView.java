package com.upper.driver.ui.activity.notification_manager;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.NotificationManager;

import java.util.List;

public interface NotificationManagerIView extends MvpView {

    void onSuccess(List<NotificationManager> managers);

    void onError(Throwable e);

}