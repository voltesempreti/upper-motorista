package com.upper.driver.ui.activity.wallet_detail;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.Transaction;

import java.util.ArrayList;

public interface WalletDetailIView extends MvpView {
    void setAdapter(ArrayList<Transaction> myList);
}
