package com.upper.driver.ui.activity.request_money;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.RequestDataResponse;

public interface RequestMoneyIView extends MvpView {

    void onSuccess(RequestDataResponse response);
    void onSuccess(Object response);
    void onError(Throwable e);

}
