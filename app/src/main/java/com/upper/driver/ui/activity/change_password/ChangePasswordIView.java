package com.upper.driver.ui.activity.change_password;

import com.upper.driver.base.MvpView;

public interface ChangePasswordIView extends MvpView {


    void onSuccess(Object object);
    void onError(Throwable e);
}
