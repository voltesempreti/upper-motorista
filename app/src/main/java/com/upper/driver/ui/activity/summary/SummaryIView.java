package com.upper.driver.ui.activity.summary;


import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.Summary;

public interface SummaryIView extends MvpView {

    void onSuccess(Summary object);

    void onError(Throwable e);
}
