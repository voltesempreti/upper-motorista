package com.upper.driver.ui.activity.forgot_password;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.ForgotResponse;

public interface ForgotIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);
    void onError(Throwable e);
}
