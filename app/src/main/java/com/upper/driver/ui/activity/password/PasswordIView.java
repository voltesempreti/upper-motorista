package com.upper.driver.ui.activity.password;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.ForgotResponse;
import com.upper.driver.data.network.model.User;

public interface PasswordIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);

    void onSuccess(User object);

    void onError(Throwable e);
}
