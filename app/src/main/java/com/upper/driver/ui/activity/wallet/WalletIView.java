package com.upper.driver.ui.activity.wallet;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.WalletMoneyAddedResponse;
import com.upper.driver.data.network.model.WalletResponse;

public interface WalletIView extends MvpView {

    void onSuccess(WalletResponse response);

    void onSuccess(WalletMoneyAddedResponse response);

    void onError(Throwable e);
}
