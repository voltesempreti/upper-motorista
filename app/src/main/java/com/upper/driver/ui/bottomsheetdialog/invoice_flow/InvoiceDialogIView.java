package com.upper.driver.ui.bottomsheetdialog.invoice_flow;

import com.upper.driver.base.MvpView;

public interface InvoiceDialogIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
