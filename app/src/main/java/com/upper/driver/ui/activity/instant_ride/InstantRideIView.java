package com.upper.driver.ui.activity.instant_ride;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.EstimateFare;
import com.upper.driver.data.network.model.TripResponse;

public interface InstantRideIView extends MvpView {

    void onSuccess(EstimateFare estimateFare);

    void onSuccess(TripResponse response);

    void onError(Throwable e);

}
