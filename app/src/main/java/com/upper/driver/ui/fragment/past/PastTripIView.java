package com.upper.driver.ui.fragment.past;


import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.HistoryList;

import java.util.List;

public interface PastTripIView extends MvpView {

    void onSuccess(List<HistoryList> historyList);
    void onError(Throwable e);
}
