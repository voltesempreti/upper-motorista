package com.upper.driver.ui.activity.sociallogin;

import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.Token;

public interface SocialLoginIView extends MvpView {

    void onSuccess(Token token);
    void onError(Throwable e);
}
