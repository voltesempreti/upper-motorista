package com.upper.driver.ui.activity.invite_friend;

import com.upper.driver.base.MvpPresenter;

public interface InviteFriendIPresenter<V extends InviteFriendIView> extends MvpPresenter<V> {
    void profile();
}
