package com.upper.driver.ui.fragment.offline;

import com.upper.driver.base.MvpView;

public interface OfflineIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
