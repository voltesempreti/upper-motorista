package com.upper.driver.ui.activity.past_detail;


import com.upper.driver.base.MvpView;
import com.upper.driver.data.network.model.HistoryDetail;

public interface PastTripDetailIView extends MvpView {

    void onSuccess(HistoryDetail historyDetail);
    void onError(Throwable e);
}
