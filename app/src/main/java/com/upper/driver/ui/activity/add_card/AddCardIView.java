package com.upper.driver.ui.activity.add_card;

import com.upper.driver.base.MvpView;

public interface AddCardIView extends MvpView {

    void onSuccess(Object card);

    void onError(Throwable e);
}
