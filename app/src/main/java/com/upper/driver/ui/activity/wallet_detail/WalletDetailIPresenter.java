package com.upper.driver.ui.activity.wallet_detail;

import com.upper.driver.base.MvpPresenter;
import com.upper.driver.data.network.model.Transaction;

import java.util.ArrayList;

public interface WalletDetailIPresenter<V extends WalletDetailIView> extends MvpPresenter<V> {
    void setAdapter(ArrayList<Transaction> myList);
}
