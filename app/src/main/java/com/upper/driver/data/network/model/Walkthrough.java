package com.upper.driver.data.network.model;

public class Walkthrough {
    public String title, description;
    public int raw;

    public Walkthrough(String title, String description, int raw) {
        this.title = title;
        this.description = description;
        this.raw = raw;
    }

}
