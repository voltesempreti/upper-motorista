/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.upper.driver;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.upper.driver";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 9;
  public static final String VERSION_NAME = "1.0.9";
  // Fields from default config.
  public static final String BASE_IMAGE_URL = "https://upper.app.br/storage/";
  public static final String BASE_PAY_URL = "https://upper.app.br/index.php";
  public static final String BASE_URL = "https://upper.app.br/";
  public static final String CLIENT_ID = "2";
  public static final String CLIENT_SECRET = "taHdKq3goXAkwavriUFZQVXbHag1AxyJseNCVgiE";
  public static final String DEVICE_TYPE = "android";
  public static final String FCM_SERRVER_KEY = "AAAAVhYC9vM:APA91bFKxE_Q1Ifw9Jri_G-4jBXML6CqN3TyDAAXMTuvThI0yn4wIQMSicEhcJVPORxabFfK1E9uhfXSjDQ56A8g2i8EaXu2amKHUQ7Yf9c_4l6RYp6lBt4flWV3exs6Fu4tsiSBH_XX";
  public static final String HELP_URL = "https://upper.app.br/help";
  public static final String TERMS_CONDITIONS = "https://upper.app.br/privacy";
  public static final String USER_PACKAGE = "com.upper.user";
}
